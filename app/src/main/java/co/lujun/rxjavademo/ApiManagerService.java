package co.lujun.rxjavademo;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Administrator on 2015/8/28.
 */
public interface ApiManagerService {
    //占位用参数必须用@Path
    //@GET("/group/{id}/users")
    //List<User> groupList(@Path("id") int groupId);
    //
    //@GET("/group/{id}/users")
    //List<User> groupList(@Path("id") int groupId, @Query("sort") String sort);
    //
    //@GET("/users/list?sort=desc")
    //
    //复杂参数可以使用Map
    //@GET("/group/{id}/users")
    //List<User> groupList(@Path("id") int groupId, @QueryMap Map<String, String> options);
    @GET("/weather")
//    WeatherData getWeather(@Query("q") String place, @Query("units") String units);//同步
    void getWeather(@Query("q") String place, @Query("units") String units, Callback<WeatherData> weatherData);//异步
}

