package co.lujun.rxjavademo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*new Thread(new Runnable() {
            @Override
            public void run() {
            //同步
                RestAdapter adapter = new RestAdapter.Builder()
                        .setEndpoint("http://api.openweathermap.org/data/2.5")
                        .build();
                ApiManagerService apiManagerService = adapter.create(ApiManagerService.class);
                WeatherData weatherData = apiManagerService.getWeather("Budapest,hu", "metric");
                Log.d("debugss", weatherData.getName());
            }
        }).start();*/

        //异步
        /*RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint("http://api.openweathermap.org/data/2.5")
                .build();
        ApiManagerService apiManagerService = adapter.create(ApiManagerService.class);
        apiManagerService.getWeather("Budapest,hu", "metric", new Callback<WeatherData>() {
            @Override
            public void success(WeatherData weatherData, Response response) {
                Log.d("debugss", weatherData.getName());
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });*/

        //利用RxJava的observable
        ApiManager.logWeatherInfo(new String[]{"beijing", "shanghai", "shenzhen", "guangzhou"});

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
