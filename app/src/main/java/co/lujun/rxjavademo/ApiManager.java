package co.lujun.rxjavademo;

import android.util.Log;

import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Administrator on 2015/8/28.
 */
public class ApiManager {

    private interface ApiManagerService{
        @GET("/weather")
        Observable<WeatherData> getWeather(@Query("q") String place, @Query("units") String units);
    }

    private static final RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint("http://api.openweathermap.org/data/2.5")
            .build();
    //
    //被观察者，事件源
    private static final ApiManagerService apiManager = restAdapter.create(ApiManagerService.class);


    /*public static Observable<WeatherData> getWeatherObserver(final String city){

        //创建Observer
        return Observable.create(new Observable.OnSubscribe<WeatherData>() {
            @Override
            public void call(Subscriber<? super WeatherData> subscriber) {
                subscriber.onNext(apiManager.getWeather(city, "metric"));
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io());
    }*/

    public static void logWeatherInfo(String[] cities){
        //被观察者发出一些列事件，观察者处理这些事件
        //
        //1. 观察者，助处理事件
        Subscriber<WeatherData> subscriber = new Subscriber<WeatherData>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(WeatherData weatherData) {
                Log.d("ApiManager", weatherData.sys.country);
            }
        };
        //观察者订阅被观察者
//        apiManager.getWeather(cities[0], "metric").subscribe(subscriber);
        //===============================================================

        //2. 使用Acion1类简化观察者
        /*apiManager.getWeather(cities[0], "metric").subscribe(new Action1<WeatherData>() {
            @Override
            public void call(WeatherData weatherData) {
                Log.d("ApiManager", weatherData.sys.country);
            }
        });*/
        //===============================================================

        //3. 操作符就是为了解决对Observable对象的变换的问题，操作符用于在Observable和最终的Subscriber之间修改Observable发出的事件
        // map,把一个事件转换为另一个事件
        // just用来创建只发出一个事件就结束的Observable对象
        /*Observable.just("Hello world!")
            .map(new Func1<String, String>() {//Func1接口函数中分别为参数类型和返回类型
                @Override
                public String call(String s) {
                    return s + "Hello Android!";//转换
                }
            }).subscribe(new Action1<String>() {
                @Override
                public void call(String s) {
                    Log.d("ApiManager", s);
                }
        });*/
        //map也可以不必返回Observable对象返回的类型，可以使用map操作符返回一个发出新的数据类型的observable对象
        /*Observable.just("Hello world!")
                .map(new Func1<String, Integer>() {//Func1接口函数中分别为参数类型和返回类型
                    @Override
                    public Integer call(String s) {
                        return s.hashCode();//转换
                    }
                }).subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer s) {
                        Log.d("ApiManager", s + "");
                    }
        });*/
        //===============================================================

        //4. from操作符，它接收一个集合作为输入，然后每次输出一个元素给subscriber
        /*Observable.from(new String[]{"hello1", "hello2", "hello3", "hello4"})
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        Log.d("ApiManager", s);
                    }
        });*/

        //===============================================================

        //5. flapMap操作符，Observable.flatMap()接收一个Observable的输出作为输入，同时输出另外一个Observable

        /*Observable.from(cities)
                .flatMap(new Func1<String, Observable<WeatherData>>() {
                    @Override
                    public Observable<WeatherData> call(String s) {
                        return apiManager.getWeather(s, "metric");
                    }
                })
                .subscribe(new Action1<WeatherData>() {
                    @Override
                    public void call(WeatherData weatherData) {
                        Log.d("ApiManager", weatherData.sys.country);
                    }
                });*/

        /*Observable
                .just(cities)
                .flatMap(new Func1<String[], Observable<String>>() {
                    @Override
                    public Observable<String> call(String[] strings) {
                        return Observable.from(strings);
                    }
                })
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        Log.d("ApiManager", s);
                    }
                });*/

        //每次只能传入一个URL，并且返回值不是一个String，而是一个输出String的Observabl对象
        /*Observable
                .just(cities)
                .flatMap(new Func1<String[], Observable<String>>() {
                    @Override
                    public Observable<String> call(String[] strings) {
                        return Observable.from(strings);
                    }
                })
                .flatMap(new Func1<String, Observable<String>>() {
                    @Override
                    public Observable<String> call(String s) {
                        return ApiManager.getTitle(s);
                    }
                })
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        Log.d("ApiManager", s);
                    }
                });*/


        //===============================================================

        //6. filter操作符，过滤
        //7. take操作符，限制最多数量
        //8. doOnNext,输出一个元素之前做一些额外的事情

        //过滤掉城市中的"shanghai"
        /*Observable
                .from(cities)
                .filter(new Func1<String, Boolean>() {
                    @Override
                    public Boolean call(String s) {
                        return s != "shanghai";
                    }
                })
                .take(2)
                .doOnNext(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        Log.d("ApiManager_doOnNext", s);
                    }
                })
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        Log.d("ApiManager", s);
                    }
                });*/



        //===============================================================

        //9.错误处理
        //每一个Observerable对象在终结的时候都会调用onCompleted()或者onError()方法
        //a.只要有异常发生onError()一定会被调用
        //b.操作符不需要处理异常
        //c.能够知道什么时候订阅者已经接收了全部的数据
        //Observable对象根本不需要知道如何处理错误,操作符也不需要处理错误状态-
        //一旦发生错误，就会跳过当前和后续的操作符。所有的错误处理都交给订阅者来做
        /*Observable
                .just("Hello World!")
                .map(new Func1<String, String>() {
                         @Override
                         public String call(String s) {
                             try {
                                 throw new Exception("My Exception!!!!");
                             }catch (Exception e){
                                 e.printStackTrace();
                             }
                             return "call_success";
                         }
                     }

                )
                .subscribe(new Subscriber<String>() {
                          @Override
                          public void onCompleted() {
                              Log.d("ApiManager", "onCompleted");
                          }

                          @Override
                          public void onError(Throwable e) {
                              Log.d("ApiManager--exception", "exception");
                          }

                          @Override
                          public void onNext(String s) {
                              Log.d("ApiManager", s);
                          }
                      }
                    );*/


        //===============================================================

        //10.调度器
        //使用subscribeOn()指定观察者代码运行的线程，使用observerOn()指定订阅者运行的线程

        //如下
        //任何在Subscriber前面执行的代码都是在I/O线程中运行。最后，操作view的代码在主线程中运行
        //可以把subscribeOn()和observerOn()添加到任何Observable对象上,这两个也是操作符
        /*Observable
                .just("Hello World!")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {

                    }
                });*/


        //===============================================================

        //11. 订阅Subscriptions
        //当调用Observable.subscribe()，会返回一个Subscription对象。这个对象代表了被观察者和订阅者之间的联系

        /*Subscription subscription = Observable
                .just("Hello World!")
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {

                    }
                });

        //可以在后面使用这个Subscription对象来操作被观察者和订阅者之间的联系
        subscription.unsubscribe();
        Log.d("ApiManager","Unsubscribed=" + subscription.isUnsubscribed());// Outputs "Unsubscribed=true"*/

    }

        private static final Observable<String> getTitle(String s){
        return Observable.just(s + "---Title");
    }
}
